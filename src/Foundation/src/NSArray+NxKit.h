//
//  NSArray+NxKit.h
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (NxKit)

/**
 *  Returns a random object from the array, or nil if the array is empty.
 *
 *  @return A random object from the array, or nil if empty.
 */
- (id)nx_randomObject;

@end
