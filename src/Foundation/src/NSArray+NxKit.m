//
//  NSArray+NxKit.m
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import "NSArray+NxKit.h"

@implementation NSArray (NxKit)

-(id)nx_randomObject
{
    // Quick out
    NSUInteger numberOfObjects = [self count];
    if (numberOfObjects == 0)
    {
        return nil;
    }
    
    return self[arc4random_uniform((unsigned int)numberOfObjects)];
}

@end
