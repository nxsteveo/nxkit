//
//  NSObject+NxKit.h
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NxKit)

/**
 *  Determines if the receiver is a kind of one of the specified classes.
 *
 *  @param firstClass A class to be used in the check.
 *  @param ... More classes to be used in the check.
 *
 *  @return YES if the receiver is a kind of one of the specified classes, otherwise NO.
 */
-(BOOL)nx_isAtLeastOneKindOfClass:(Class)firstClass, ... NS_REQUIRES_NIL_TERMINATION;

@end
