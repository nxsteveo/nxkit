//
//  NSObject+NxKit.m
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import "NSObject+NxKit.h"

@implementation NSObject (NxKit)

-(BOOL)nx_isAtLeastOneKindOfClass:(Class)firstClass, ...
{
    va_list classes;
    va_start(classes, firstClass);
    
    BOOL isKindOfClass = NO;
    
    for (Class cls = firstClass; cls != nil; cls = va_arg(classes, Class))
    {
        if ([self isKindOfClass:cls])
        {
            isKindOfClass = YES;
            break;
        }
    }
    
    va_end(classes);
    
    return isKindOfClass;
}

@end
