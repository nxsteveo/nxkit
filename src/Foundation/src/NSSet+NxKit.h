//
//  NSSet+NxKit.h
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSSet (NxKit)

/**
 *  Returns a random object from the set, or nil if the set is empty.
 *
 *  @return A random object from the set, or nil if empty.
 */
-(id)nx_randomObject;

@end
