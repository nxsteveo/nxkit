//
//  NSSet+NxKit.m
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import "NSSet+NxKit.h"
#import "NSArray+NxKit.h"

@implementation NSSet (NxKit)

-(id)nx_randomObject
{
    return [[self allObjects] nx_randomObject];
}

@end
