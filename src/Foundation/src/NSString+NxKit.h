//
//  NSString+NxKit.h
//  NxKit
//
//  Created by Steve Wilford on 21/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NxKit)

/**
 *  Returns an NSString object initialized by converting given data into Unicode characters using a given encoding
 *
 *  @param data     An NSData object containing bytes in \c encoding and the default plain text format (that is, pure content with no attributes or other markups) for that encoding.
 *  @param encoding The encoding used by \c data.
 *
 *  @return An NSString object initialized by converting the bytes in data into Unicode characters using encoding. Returns nil if the initialization fails for some reason (for example if data does not represent valid data for encoding).
 */
+ (NSString *)nx_stringWithData:(NSData *)data encoding:(NSStringEncoding)encoding;

/**
 *  Finds the range of the first occurrence of a given string within the receiver, subject to given options.
 *
 *  @param substring The string to search for. This value must not be \c nil.
 
 \b Important Raises an \c NSInvalidArgumentException if \c substring is nil.
 *
 *  @return An \c NSRange structure giving the location and length in the receiver of the first occurrence of \c substring. Returns \c {NSNotFound, 0} if \c substring is not found or is empty (@"").
 */
- (BOOL)nx_containsString:(NSString *)substring;

/**
 *  Finds the range of the first occurrence of a given string within the receiver, subject to given options.
 *
 *  @param substring The string to search for. This value must not be \c nil.
 
     \b Important Raises an \c NSInvalidArgumentException if \c substring is nil.
 *  @param options   A mask specifying search options. The following options may be specified by combining them with the C bitwise OR operator: \c NSCaseInsensitiveSearch, \c NSLiteralSearch, \c NSBackwardsSearch, \c NSAnchoredSearch. See String Programming Guide for details on these options.
 *
 *  @return An \c NSRange structure giving the location and length in the receiver of the first occurrence of \c substring, modulo the options in mask. Returns \c {NSNotFound, 0} if \c substring is not found or is empty (@"").
 */
- (BOOL)nx_containsString:(NSString *)substring options:(NSStringCompareOptions)options;

@end
