//
//  NSString+NxKit.m
//  NxKit
//
//  Created by Steve Wilford on 21/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import "NSString+NxKit.h"

@implementation NSString (NxKit)

+(NSString *)nx_stringWithData:(NSData *)data encoding:(NSStringEncoding)encoding
{
    return [[self alloc] initWithData:data encoding:encoding];
}

-(BOOL)nx_containsString:(NSString *)substring
{
    return [self nx_containsString:substring options:0];
}

-(BOOL)nx_containsString:(NSString *)substring options:(NSStringCompareOptions)options
{
    return [self rangeOfString:substring options:options].location != NSNotFound;
}

@end
