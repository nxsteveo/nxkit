//
//  NSString+NxKitRegex.h
//  NxKit
//
//  Created by Steve Wilford on 05/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NxKitRegex)

/**
 *  Returns an array containing all the matches of the regular expression in the string.
 *
 *  @param pattern A regular expression pattern
 *  @param error   An out value that returns any error encountered during initialization. Returns an \c NSError object if the regular expression pattern is invalid; otherwise returns \c nil.
 *
 *  @return An array of matches.
 */
-(NSArray *)nx_matchesWithRegularExpressionPattern:(NSString *)pattern
                                             error:(NSError **)error;

/**
 *  Returns an array containing all the matches of the regular expression in the string.
 *
 *  @param pattern         A regular expression pattern
 *  @param patternOptions  The matching options. See NSRegularExpressionOptions for possible values. The values can be combined using the C-bitwise OR operator.
 *  @param range           The range of the string to search.
 *  @param matchingOptions The matching options to use. See NSMatchingOptions for possible values.
 *  @param error           An out value that returns any error encountered during initialization. Returns an \c NSError object if the regular expression pattern is invalid; otherwise returns \c nil.
 *  @return An array of matches.
 */
-(NSArray *)nx_matchesWithRegularExpressionPattern:(NSString *)pattern
                                    patternOptions:(NSRegularExpressionOptions)patternOptions
                                             range:(NSRange)range
                                   matchingOptions:(NSMatchingOptions)matchingOptions
                                             error:(NSError **)error;

/**
 *  Returns the number of matches of the regular expression pattern within the specified range of the string
 *
 *  @param pattern A regular expression pattern
 *  @param error   An out value that returns any error encountered during initialization. Returns an \c NSError object if the regular expression pattern is invalid; otherwise returns \c nil.
 *
 *  @return The number of matches of the regular expression pattern.
 */
-(NSUInteger)nx_numberOfMatchesWithRegularExpressionPattern:(NSString *)pattern
                                                      error:(NSError **)error;

/**
 *  Returns the number of matches of the regular expression pattern within the specified range of the string
 *
 *  @param pattern         A regular expression pattern
 *  @param patternOptions  The matching options. See NSRegularExpressionOptions for possible values. The values can be combined using the C-bitwise OR operator.
 *  @param range           The range of the string to search.
 *  @param matchingOptions The matching options to use. See NSMatchingOptions for possible values.
 *  @param error           An out value that returns any error encountered during initialization. Returns an \c NSError object if the regular expression pattern is invalid; otherwise returns \c nil.
 *
 *  @return The number of matches of the regular expression pattern.
 */
-(NSUInteger)nx_numberOfMatchesWithRegularExpressionPattern:(NSString *)pattern
                                             patternOptions:(NSRegularExpressionOptions)patternOptions
                                                      range:(NSRange)range
                                            matchingOptions:(NSMatchingOptions)matchingOptions
                                                      error:(NSError *__autoreleasing *)error;

/**
 *  Returns the first match of the regular expression pattern within the specified range of the string.
 *
 *  @param pattern A regular expression pattern
 *  @param error   An out value that returns any error encountered during initialization. Returns an \c NSError object if the regular expression pattern is invalid; otherwise returns \c nil.
 *
 *  @return The first match.
 */
-(NSTextCheckingResult *)nx_firstMatchWithRegularExpressionPattern:(NSString *)pattern
                                                             error:(NSError **)error;

/**
 *  Returns the first match of the regular expression pattern within the specified range of the string.
 *
 *  @param pattern         A regular expression pattern
 *  @param patternOptions  The matching options. See NSRegularExpressionOptions for possible values. The values can be combined using the C-bitwise OR operator.
 *  @param range           The range of the string to search.
 *  @param matchingOptions The matching options to use. See NSMatchingOptions for possible values.
 *  @param error           An out value that returns any error encountered during initialization. Returns an \c NSError object if the regular expression pattern is invalid; otherwise returns \c nil.
 *
 *  @return The first match.
 */
-(NSTextCheckingResult *)nx_firstMatchWithRegularExpressionPattern:(NSString *)pattern
                                                    patternOptions:(NSRegularExpressionOptions)patternOptions
                                                             range:(NSRange)range
                                                   matchingOptions:(NSMatchingOptions)matchingOptions
                                                             error:(NSError **)error;

/**
 *  Returns the range of the first match of the regular expression pattern within the specified range of the string.
 *
 *  @param pattern A regular expression pattern
 *  @param error   An out value that returns any error encountered during initialization. Returns an \c NSError object if the regular expression pattern is invalid; otherwise returns \c nil.
 *
 *  @return The range of the first match. Returns {NSNotFound, 0} if no match is found.
 */
-(NSRange)nx_rangeOfFirstMatchWithRegularExpressionPattern:(NSString *)pattern
                                                     error:(NSError **)error;

/**
 *  Returns the range of the first match of the regular expression pattern within the specified range of the string.
 *
 *  @param pattern         A regular expression pattern
 *  @param patternOptions  The matching options. See NSRegularExpressionOptions for possible values. The values can be combined using the C-bitwise OR operator.
 *  @param range           The range of the string to search.
 *  @param matchingOptions The matching options to use. See NSMatchingOptions for possible values.
 *  @param error           An out value that returns any error encountered during initialization. Returns an \c NSError object if the regular expression pattern is invalid; otherwise returns \c nil.
 *
 *  @return The range of the first match. Returns {NSNotFound, 0} if no match is found.
 */
-(NSRange)nx_rangeOfFirstMatchWithRegularExpressionPattern:(NSString *)pattern
                                            patternOptions:(NSRegularExpressionOptions)patternOptions
                                                     range:(NSRange)range
                                           matchingOptions:(NSMatchingOptions)matchingOptions
                                                     error:(NSError **)error;

@end
