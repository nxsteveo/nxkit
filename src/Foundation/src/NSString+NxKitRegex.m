//
//  NSString+NxKitRegex.m
//  NxKit
//
//  Created by Steve Wilford on 05/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import "NSString+NxKitRegex.h"

@implementation NSString (NxKitRegex)

-(NSArray *)nx_matchesWithRegularExpressionPattern:(NSString *)pattern
                                             error:(NSError *__autoreleasing *)error
{
    return [self nx_matchesWithRegularExpressionPattern:pattern
                                         patternOptions:0
                                                  range:NSMakeRange(0, [self length])
                                        matchingOptions:0
                                                  error:error];
}

-(NSArray *)nx_matchesWithRegularExpressionPattern:(NSString *)pattern
                                    patternOptions:(NSRegularExpressionOptions)patternOptions
                                             range:(NSRange)range
                                   matchingOptions:(NSMatchingOptions)matchingOptions
                                             error:(NSError *__autoreleasing *)error
{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:patternOptions
                                                                             error:error];
    
    if (*error)
    {
        return nil;
    }
    return [regex matchesInString:self options:matchingOptions range:range];
}

-(NSUInteger)nx_numberOfMatchesWithRegularExpressionPattern:(NSString *)pattern
                                                      error:(NSError *__autoreleasing *)error
{
    return [self nx_numberOfMatchesWithRegularExpressionPattern:pattern
                                                 patternOptions:0
                                                          range:NSMakeRange(0, [self length])
                                                matchingOptions:0
                                                          error:error];
}

-(NSUInteger)nx_numberOfMatchesWithRegularExpressionPattern:(NSString *)pattern
                                             patternOptions:(NSRegularExpressionOptions)patternOptions
                                                      range:(NSRange)range
                                            matchingOptions:(NSMatchingOptions)matchingOptions
                                                      error:(NSError *__autoreleasing *)error
{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:patternOptions
                                                                             error:error];
    
    if (*error)
    {
        return 0;
    }
    return [regex numberOfMatchesInString:self options:matchingOptions range:range];
}

-(NSTextCheckingResult *)nx_firstMatchWithRegularExpressionPattern:(NSString *)pattern
                                                             error:(NSError *__autoreleasing *)error
{
    return [self nx_firstMatchWithRegularExpressionPattern:pattern
                                            patternOptions:0
                                                     range:NSMakeRange(0, [self length])
                                           matchingOptions:0
                                                     error:error];
}

-(NSTextCheckingResult *)nx_firstMatchWithRegularExpressionPattern:(NSString *)pattern
                                                    patternOptions:(NSRegularExpressionOptions)patternOptions
                                                             range:(NSRange)range
                                                   matchingOptions:(NSMatchingOptions)matchingOptions
                                                             error:(NSError *__autoreleasing *)error
{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:patternOptions
                                                                             error:error];
    
    if (*error)
    {
        return nil;
    }
    return [regex firstMatchInString:self options:matchingOptions range:range];
}

-(NSRange)nx_rangeOfFirstMatchWithRegularExpressionPattern:(NSString *)pattern
                                                     error:(NSError *__autoreleasing *)error
{
    return [self nx_rangeOfFirstMatchWithRegularExpressionPattern:pattern
                                                   patternOptions:0
                                                            range:NSMakeRange(0, [self length])
                                                  matchingOptions:0
                                                            error:error];
}

-(NSRange)nx_rangeOfFirstMatchWithRegularExpressionPattern:(NSString *)pattern
                                            patternOptions:(NSRegularExpressionOptions)patternOptions
                                                     range:(NSRange)range
                                           matchingOptions:(NSMatchingOptions)matchingOptions
                                                     error:(NSError *__autoreleasing *)error
{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:patternOptions
                                                                             error:error];
    
    if (*error)
    {
        return NSMakeRange(NSNotFound, 0);
    }
    return [regex rangeOfFirstMatchInString:self options:matchingOptions range:range];
}

@end
