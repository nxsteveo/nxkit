//
//  NSURL+NxKit.h
//  NxKit
//
//  Created by Steve Wilford on 05/05/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (NxKit)

+ (instancetype)nx_URLWithFormat:(NSString *)format, ...;

- (instancetype)nx_initWithFormat:(NSString *)format, ...;

@end
