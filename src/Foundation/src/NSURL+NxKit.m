//
//  NSURL+NxKit.m
//  NxKit
//
//  Created by Steve Wilford on 05/05/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import "NSURL+NxKit.h"

@implementation NSURL (NxKit)

+ (instancetype)nx_URLWithFormat:(NSString *)format, ...
{
    va_list args;
    va_start(args, format);
    NSString *url = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    return [self URLWithString:url];
}

- (instancetype)nx_initWithFormat:(NSString *)format, ...
{
    va_list args;
    va_start(args, format);
    NSString *url = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    return [self initWithString:url];
}

@end
