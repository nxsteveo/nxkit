//
//  NxQueue.h
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NxQueue : NSObject

/**
 *  Creates an empty queue
 *
 *  @return An initialized queue.
 */
+ (instancetype)queue;

/**
 *  Creates a queue, initialized with enough memory to initially hold a given number of objects.
 *
 *  Queues expand as needed; \c capacity simply establishes the object’s initial capacity.
 *
 *  @param capacity The initial capacity of the new queue.
 *
 *  @return A queue initialized with enough memory to hold \c capacity objects.
 */
+ (instancetype)queueWithCapacity:(NSUInteger)capacity;

/**
 *  Creates and returns a queue containing the objects in the argument list.
 * 
 *  The first object in the list is the first one that will be dequeued.
 *
 *  @param firstObject A comma-separated list of objects ending with nil.
 *
 *  @return A queue initialized with the provided objects.
 */
+ (instancetype)queueWithObjects:(id)firstObject, ... NS_REQUIRES_NIL_TERMINATION;

/**
 *  Returns the number of objects in the queue.
 *
 *  @return The number of objects in the queue.
 */
-(NSUInteger)count;

/**
 *  Enqueues the provided object onto the queue.
 *
 *  @param anObject
 The object to enqueue to the queue. This value must not be nil.

\b Important Raises an NSInvalidArgumentException if \c anObject is nil.
 */
-(void)enqueue:(id)anObject;

/**
 *  Removes the next object in the queue and returns it.
 *
 *  @return The next object in the queue, or nil if the queue is empty.
 */
-(id)dequeue;

/**
 *  Returns the next object in the queue without removing it.
 *
 *  @return The next object in the queue, or nil if the queue is empty.
 */
-(id)peek;

@end
