//
//  NxQueue.m
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import "NxQueue.h"

@interface NxQueue ()
@property (nonatomic, strong) NSMutableArray *internalStorage;
@end

@implementation NxQueue

+(instancetype)queue
{
    return [[self alloc] init];
}

+(instancetype)queueWithCapacity:(NSUInteger)capacity
{
    NxQueue *queue = [self queue];
    queue.internalStorage = [NSMutableArray arrayWithCapacity:capacity];
    return queue;
}

+(instancetype)queueWithObjects:(id)firstObject, ...
{
    // Thanks to Matt Gallagher at http://www.cocoawithlove.com/2009/05/variable-argument-lists-in-cocoa.html

    NSMutableArray *objects = [NSMutableArray array];
    
    va_list args;
    va_start(args, firstObject);
    for (id obj = firstObject; obj != nil; obj = va_arg(args, id))
    {
        [objects addObject:obj];
    }
    
    NxQueue *queue = [self queue];
    queue.internalStorage = objects;
    return queue;
}

-(NSUInteger)count
{
    return [self.internalStorage count];
}

-(void)enqueue:(id)anObject
{
    [self.internalStorage addObject:anObject];
}

-(id)dequeue
{
    id object = [self peek];
    if (object)
    {
        [self.internalStorage removeObjectAtIndex:0];
    }
    return object;
}

-(id)peek
{
    return [self.internalStorage firstObject];
}

@end
