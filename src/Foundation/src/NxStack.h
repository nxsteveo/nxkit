//
//  NxStack.h
//  NxKit
//
//  Created by Steve Wilford on 05/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NxStack : NSObject

/**
 *  Creates an empty stack
 *
 *  @return An initialized stack.
 */
+ (instancetype)stack;

/**
 *  Creates a stack, initialized with enough memory to initially hold a given number of objects.
 *
 *  Stacks expand as needed; \c capacity simply establishes the object’s initial capacity.
 *
 *  @param capacity The initial capacity of the new stack.
 *
 *  @return A stack initialized with enough memory to hold \c capacity objects.
 */
+ (instancetype)stackWithCapacity:(NSUInteger)capacity;

/**
 *  Creates and returns a stack containing the objects in the argument list.
 *
 *  The last object in the list is the first one that will be popped.
 *
 *  @param firstObject A comma-separated list of objects ending with nil.
 *
 *  @return A stack initialized with the provided objects.
 */
+ (instancetype)stackWithObjects:(id)firstObject, ... NS_REQUIRES_NIL_TERMINATION;

/**
 *  Returns the number of objects in the stack.
 *
 *  @return The number of objects in the stack.
 */
-(NSUInteger)count;

/**
 *  Pushes the provided object onto the stack.
 *
 *  @param anObject
 The object to push to the stack. This value must not be nil.
 
 \b Important Raises an NSInvalidArgumentException if \c anObject is nil.
 */
-(void)push:(id)anObject;

/**
 *  Removes the next object in the stack and returns it.
 *
 *  @return The next object in the stack, or nil if the stack is empty.
 */
-(id)pop;

/**
 *  Returns the next object in the stack without removing it.
 *
 *  @return The next object in the stack, or nil if the stack is empty.
 */
-(id)peek;

@end
