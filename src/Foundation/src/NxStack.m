//
//  NxStack.m
//  NxKit
//
//  Created by Steve Wilford on 05/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import "NxStack.h"

@interface NxStack ()
@property (nonatomic, strong) NSMutableArray *internalStorage;
@end

@implementation NxStack

+(instancetype)stack
{
    return [[self alloc] init];
}

+(instancetype)stackWithCapacity:(NSUInteger)capacity
{
    NxStack *stack = [self stack];
    stack.internalStorage = [NSMutableArray arrayWithCapacity:capacity];
    return stack;
}

+(instancetype)stackWithObjects:(id)firstObject, ...
{
    // Thanks to Matt Gallagher at http://www.cocoawithlove.com/2009/05/variable-argument-lists-in-cocoa.html
    
    NSMutableArray *objects = [NSMutableArray array];
    
    va_list args;
    va_start(args, firstObject);
    for (id obj = firstObject; obj != nil; obj = va_arg(args, id))
    {
        [objects addObject:obj];
    }
    
    NxStack *stack = [self stack];
    stack.internalStorage = objects;
    return stack;
}

-(NSUInteger)count
{
    return [self.internalStorage count];
}

-(void)push:(id)anObject
{
    [self.internalStorage addObject:anObject];
}

-(id)pop
{
    id object = [self peek];
    if (object)
    {
        [self.internalStorage removeLastObject];
    }
    return object;
}

-(id)peek
{
    return [self.internalStorage lastObject];
}

@end
