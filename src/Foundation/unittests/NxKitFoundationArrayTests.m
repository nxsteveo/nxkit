//
//  NxKitFoundationArrayTests.m
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSArray+NxKit.h"

@interface NxKitFoundationArrayTests : XCTestCase

@end

static NSMutableArray *largeArray;

@implementation NxKitFoundationArrayTests

+(void)setUp
{
    [super setUp];
    
    int start = 1000000;
    int limit = 9999999;
    
    largeArray = [NSMutableArray array];
    for (int i = start; i < start + limit; ++i)
    {
        [largeArray addObject:@(i)];
    }
}

/**
 *  This tests an array containing objects, removing them until none remain.
 */
-(void)testRandom
{
    NSMutableArray *arrayToTest = [@[@"A", @"B", @"C", @"D", @"E", @"F", @"G"] mutableCopy];
    
    // Pull out a random object until no more remain
    id object = nil;
    do
    {
        // Get a random object and make sure it exists in the array
        object = [arrayToTest nx_randomObject];
        if ([arrayToTest containsObject:object] == NO)
        {
            XCTAssertNotNil(object, @"Could not retrieve random object");
            break;
        }
        
        // Remove the object from the array
        [arrayToTest removeObject:object];
    } while ([arrayToTest count] != 0);
}

/**
 *  This tests an array with a large amount of objects
 */
-(void)testRandomLarge
{
    NSArray *arrayToTest = largeArray;
    XCTAssertNotNil([arrayToTest nx_randomObject], @"Could not retrieve random object");
}

@end
