//
//  NxKitFoundationObjectTests.m
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSObject+NxKit.h"

@interface NxKitFoundationObjectTests : XCTestCase

@end

@implementation NxKitFoundationObjectTests

- (void)testKindOfClasses
{
    id someObject = @(1);
    BOOL result = [someObject nx_isAtLeastOneKindOfClass:[NSString class], [NSNumber class], nil];
    XCTAssertTrue(result, @"Object is not an NSString or an NSNumber");
    
    someObject = @"1";
    result = [someObject nx_isAtLeastOneKindOfClass:[NSString class], nil];
    XCTAssertTrue(result, @"Object is not an NSString");
    
    result = [someObject nx_isAtLeastOneKindOfClass:[NSNumber class], nil];
    XCTAssertFalse(result, @"Object is an NSNumber");
}

@end
