//
//  NxKitFoundationQueueTests.m
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NxQueue.h"

@interface NxKitFoundationQueueTests : XCTestCase
@property (nonatomic, strong) NxQueue *queue;
@end

@implementation NxKitFoundationQueueTests

-(void)setUp
{
    [super setUp];
    
    self.queue = [NxQueue queueWithObjects:@"1", @"2", @"3", @"4", nil];
}

- (void)testCreationWithoutObjects
{
    NxQueue *queue = [NxQueue queue];
    XCTAssertNotNil(queue, @"Queue could not be created");
    XCTAssert([queue count] == 0, @"Queue size is incorrect");
}

- (void)testCreationWithObjects
{
    NxQueue *queue = [NxQueue queueWithObjects:@"1", @"2", @"3", @"4", nil];
    XCTAssert([queue count] == 4, @"Queue could not be created with objects");
}

- (void)testCreationWithCapacity
{
    NxQueue *queue = [NxQueue queueWithCapacity:4];
    XCTAssertNotNil(queue, @"Queue could not be created with capacity");
}

- (void)testPeek
{
    NSUInteger countBeforePeek = [self.queue count];
    NSString *string = [self.queue peek];
    NSUInteger countAfterPeek = [self.queue count];
    XCTAssert(countBeforePeek == countAfterPeek, @"Queue has been modified");
    XCTAssert([string isEqualToString:@"1"], @"Next object could not be inspected");
}

- (void)testSingleDequeue
{
    NSUInteger countBeforeDequeue = [self.queue count];
    NSString *string = [self.queue dequeue];
    NSUInteger countAfterDequeue = [self.queue count];
    XCTAssert(countAfterDequeue == countBeforeDequeue-1, @"Incorrect queue size reported");
    XCTAssert([string isEqualToString:@"1"], @"Next object could not be dequeued");
}

- (void)testSingleEnqueue
{
    NSUInteger countBeforeEnqueue = [self.queue count];
    [self.queue enqueue:@"5"];
    NSUInteger countAfterEnqueue = [self.queue count];
    XCTAssert(countAfterEnqueue == countBeforeEnqueue+1, @"Object could not be enqueued");
}

- (void)testMultipleDequeue
{
    while ([self.queue count] > 0)
    {
        NSString *string = [self.queue dequeue];
        XCTAssertNotNil(string, @"Next object could not be dequeued");
    }
    
    XCTAssert([self.queue count] == 0, @"Could not dequeue all objects");
}

- (void)testMultipleEnqueue
{
    const NSUInteger numberOfAdditionalObjectsToEnqueue = 5;
    
    NSUInteger countBeforeEnqueue = [self.queue count];
    
    // Enqueue some additional objects
    for (NSUInteger i=0; i < numberOfAdditionalObjectsToEnqueue; ++i)
    {
        [self.queue enqueue:[NSString stringWithFormat:@"Additional %ld", (unsigned long)i]];
    }
    
    NSUInteger countAfterEnqueue = [self.queue count];
    XCTAssert(countAfterEnqueue == countBeforeEnqueue + numberOfAdditionalObjectsToEnqueue, @"Could not enqueue all objects");
}

@end
