//
//  NxKitFoundationRegexTests.m
//  NxKit
//
//  Created by Steve Wilford on 05/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+NxKitRegex.h"

@interface NxKitFoundationRegexTests : XCTestCase
@property (nonatomic, copy) NSString *stringToTest;
@end

@implementation NxKitFoundationRegexTests

- (void)setUp
{
    [super setUp];
    
    self.stringToTest = @"Test string for regex";
}

- (void)testSimpleRegexMatches
{
    NSError *error = nil;
    NSArray *matches = [self.stringToTest nx_matchesWithRegularExpressionPattern:@"for"
                                                                           error:&error];
    
    XCTAssertNil(error, @"Regular expression creation failed");
    XCTAssert([matches count] == 1, @"Pattern matched failed");
}

- (void)testCustomRegexMatches
{
    NSError *error = nil;
    NSArray *matches = [self.stringToTest nx_matchesWithRegularExpressionPattern:@"FOR"
                                                                  patternOptions:NSRegularExpressionCaseInsensitive
                                                                           range:NSMakeRange(0, [self.stringToTest length])
                                                                 matchingOptions:0
                                                                           error:&error];
    
    XCTAssertNil(error, @"Regular expression creation failed");
    XCTAssert([matches count] == 1, @"Pattern matched failed");
}

- (void)testSimpleNumberOfMatches
{
    NSError *error = nil;
    NSUInteger numberOfMatches = [self.stringToTest nx_numberOfMatchesWithRegularExpressionPattern:@"for"
                                                                                             error:&error];
    
    XCTAssertNil(error, @"Regular expression creation failed");
    XCTAssert(numberOfMatches == 1, @"Pattern match failed");
}

- (void)testCustomNumberOfMatches
{
    NSError *error = nil;
    NSUInteger numberOfMatches = [self.stringToTest nx_numberOfMatchesWithRegularExpressionPattern:@"E"
                                                                                    patternOptions:NSRegularExpressionCaseInsensitive
                                                                                             range:NSMakeRange(0, [self.stringToTest length])
                                                                                   matchingOptions:0
                                                                                             error:&error];
    
    XCTAssertNil(error, @"Regular expression creation failed");
    XCTAssert(numberOfMatches == 3, @"Pattern match failed");
}

- (void)testSimpleFirstMatch
{
    NSError *error = nil;
    NSTextCheckingResult *firstMatch = [self.stringToTest nx_firstMatchWithRegularExpressionPattern:@"s"
                                                                                              error:&error];
    
    XCTAssertNil(error, @"Regular expression creation failed");
    XCTAssert(firstMatch.range.location == 2, @"First match found at unexpected location");
}

- (void)testCustomFirstMatch
{
    NSError *error = nil;
    NSTextCheckingResult *firstMatch = [self.stringToTest nx_firstMatchWithRegularExpressionPattern:@"S"
                                                                                     patternOptions:NSRegularExpressionCaseInsensitive
                                                                                              range:NSMakeRange(0, [self.stringToTest length])
                                                                                    matchingOptions:0
                                                                                              error:&error];
    
    XCTAssertNil(error, @"Regular expression creation failed");
    XCTAssert(firstMatch.range.location == 2, @"First match found at unexpected location");
}

- (void)testSimpleRangeOfFirstMatch
{
    NSError *error = nil;
    NSRange rangeOfFirstMatch = [self.stringToTest nx_rangeOfFirstMatchWithRegularExpressionPattern:@"s"
                                                                                              error:&error];
    
    XCTAssertNil(error, @"Regular expression creation failed");
    XCTAssert(rangeOfFirstMatch.location == 2, @"First match found at unexpected location");
}

- (void)testCustomRangeOfFirstMatch
{
    NSError *error = nil;
    NSRange rangeOfFirstMatch = [self.stringToTest nx_rangeOfFirstMatchWithRegularExpressionPattern:@"S"
                                                                                     patternOptions:NSRegularExpressionCaseInsensitive
                                                                                              range:NSMakeRange(0, [self.stringToTest length])
                                                                                    matchingOptions:0
                                                                                              error:&error];
    
    XCTAssertNil(error, @"Regular expression creation failed");
    XCTAssert(rangeOfFirstMatch.location == 2, @"First match found at unexpected location");
}

@end
