//
//  NxKitFoundationSetTests.m
//  NxKit
//
//  Created by Steve Wilford on 04/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSSet+NxKit.h"

@interface NxKitFoundationSetTests : XCTestCase

@end

static NSMutableSet *largeSet;

@implementation NxKitFoundationSetTests

+(void)setUp
{
    [super setUp];
    
    int start = 1000000;
    int limit = 9999999;
    
    largeSet = [NSMutableSet set];
    
    for (int i = start; i < start + limit; ++i)
    {
        [largeSet addObject:@(i)];
    }
}

/**
 *  This tests a set containing objects, removing them until none remain.
 */
- (void)testRandom
{
    NSMutableSet *setToTest = [NSMutableSet setWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", nil];

    // Pull out a random object until no more remain
    id object = nil;
    do
    {
        // Get a random object and make sure it exists in the array
        object = [setToTest nx_randomObject];
        if ([setToTest containsObject:object] == NO)
        {
            XCTAssertNotNil(object, @"Could not retrieve random object");
            break;
        }
        
        // Remove the object from the set
        [setToTest removeObject:object];
    } while ([setToTest count] != 0);
}

/**
 *  This tests a set with a large amount of objects
 */
-(void)testRandomLarge
{
    NSSet *setToTest = largeSet;
    XCTAssertNotNil([setToTest nx_randomObject], @"Could not retrieve random object");
}

@end
