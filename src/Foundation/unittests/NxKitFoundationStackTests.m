//
//  NxKitFoundationStackTests.m
//  NxKit
//
//  Created by Steve Wilford on 05/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NxStack.h"

@interface NxKitFoundationStackTests : XCTestCase
@property (nonatomic, strong) NxStack *stack;
@end

@implementation NxKitFoundationStackTests

-(void)setUp
{
    [super setUp];
    
    self.stack = [NxStack stackWithObjects:@"1", @"2", @"3", @"4", nil];
}

- (void)testCreationWithoutObjects
{
    NxStack *stack = [NxStack stack];
    XCTAssertNotNil(stack, @"Stack could not be created");
    XCTAssert([stack count] == 0, @"Stack size is incorrect");
}

- (void)testCreationWithObjects
{
    NxStack *stack = [NxStack stackWithObjects:@"1", @"2", @"3", @"4", nil];
    XCTAssert([stack count] == 4, @"Stack could not be created with objects");
}

- (void)testCreationWithCapacity
{
    NxStack *stack = [NxStack stackWithCapacity:4];
    XCTAssertNotNil(stack, @"Stack could not be created with capacity");
}

- (void)testPeek
{
    NSUInteger countBeforePeek = [self.stack count];
    NSString *string = [self.stack peek];
    NSUInteger countAfterPeek = [self.stack count];
    XCTAssert(countBeforePeek == countAfterPeek, @"Stack has been modified");
    XCTAssert([string isEqualToString:@"4"], @"Next object could not be inspected");
}

- (void)testSinglePop
{
    NSUInteger countBeforePop = [self.stack count];
    NSString *string = [self.stack pop];
    NSUInteger countAfterPop = [self.stack count];
    XCTAssert(countAfterPop == countBeforePop-1, @"Incorrect stack size reported");
    XCTAssert([string isEqualToString:@"4"], @"Next object could not be popped");
}

- (void)testSinglePush
{
    NSUInteger countBeforePush = [self.stack count];
    [self.stack push:@"5"];
    NSUInteger countAfterPush = [self.stack count];
    XCTAssert(countAfterPush == countBeforePush+1, @"Object could not be pushed");
}

- (void)testMultiplePop
{
    while ([self.stack count] > 0)
    {
        NSString *string = [self.stack pop];
        XCTAssertNotNil(string, @"Next object could not be popped");
    }
    
    XCTAssert([self.stack count] == 0, @"Could not pop all objects");
}

- (void)testMultiplePush
{
    const NSUInteger numberOfAdditionalObjectsToPush = 5;
    
    NSUInteger countBeforePush = [self.stack count];
    
    // Push some additional objects
    for (NSUInteger i=0; i < numberOfAdditionalObjectsToPush; ++i)
    {
        [self.stack push:[NSString stringWithFormat:@"Additional %ld", (unsigned long)i]];
    }
    
    NSUInteger countAfterPush = [self.stack count];
    XCTAssert(countAfterPush == countBeforePush + numberOfAdditionalObjectsToPush, @"Could not push all objects");
}

@end
