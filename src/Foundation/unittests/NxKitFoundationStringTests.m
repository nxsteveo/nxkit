//
//  NxKitFoundationStringTests.m
//  NxKit
//
//  Created by Steve Wilford on 21/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+NxKit.h"

@interface NxKitFoundationStringTests : XCTestCase

@end

@implementation NxKitFoundationStringTests

- (void)testStringWithData
{
    const char *c = "Test string";
    NSData *data = [NSData dataWithBytes:c length:strlen(c)];
    
    NSString *builtInString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *nxKitString = [NSString nx_stringWithData:data encoding:NSUTF8StringEncoding];
    
    if ([nxKitString isEqualToString:builtInString] == NO)
    {
        XCTFail(@"The string was not created correctly.");
    }
}

- (void)testStringSearch
{
    NSString *stringToSearch = @"A string to be used for searching tests.";
    
    BOOL stringFoundWithCorrectCase = [stringToSearch nx_containsString:@"st"];
    BOOL stringFoundWithIncorrectCase = [stringToSearch nx_containsString:@"ST"];
    BOOL stringFoundWithIncorrectSubstring = [stringToSearch nx_containsString:@"word"];
    
    XCTAssertTrue(stringFoundWithCorrectCase, @"Failed to find substring.");
    XCTAssertFalse(stringFoundWithIncorrectCase, @"Incorrectly found cased substring.");
    XCTAssertFalse(stringFoundWithIncorrectSubstring, @"Incorrectly found substring.");
}

- (void)testStringSearchWithOptions
{
    NSString *stringToSearch = @"A string to be used for searching tests.";
    
    BOOL stringFoundWithCorrectCase = [stringToSearch nx_containsString:@"st"
                                                                options:NSCaseInsensitiveSearch];
    BOOL stringFoundWithIncorrectCase = [stringToSearch nx_containsString:@"ST"
                                                                  options:NSCaseInsensitiveSearch];
    BOOL stringFoundWithIncorrectSubstring = [stringToSearch nx_containsString:@"word"
                                                                       options:NSCaseInsensitiveSearch];
    
    XCTAssertTrue(stringFoundWithCorrectCase, @"Failed to find substring.");
    XCTAssertTrue(stringFoundWithIncorrectCase, @"Failed to find cased substring.");
    XCTAssertFalse(stringFoundWithIncorrectSubstring, @"Incorrectly found substring.");
}

@end
