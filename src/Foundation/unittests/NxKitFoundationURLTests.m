//
//  NxKitFoundationURLTests.m
//  NxKit
//
//  Created by Steve Wilford on 05/05/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSURL+NxKit.h"

@interface NxKitFoundationURLTests : XCTestCase

@end

@implementation NxKitFoundationURLTests

- (void)testClassMethodInstantiation
{
    NSString *host = @"github.com";
    NSURL *URL = [NSURL nx_URLWithFormat:@"https://%@/NxSoftware", host];
    
    XCTAssertTrue([[URL host] isEqualToString:host], @"Host is incorrect");
}

- (void)testInstanceMethodInstantiation
{
    NSString *host = @"github.com";
    NSURL *URL = [[NSURL alloc] nx_initWithFormat:@"https://%@/NxSoftware", host];
    
    XCTAssertTrue([[URL host] isEqualToString:host], @"Host is incorrect");
}

@end
