//
//  SKProduct+NxKit.h
//  NxKit
//
//  Created by Steve Wilford on 16/02/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <StoreKit/StoreKit.h>

@interface SKProduct (NxKit)

- (NSString *)nx_localizedPrice;

@end
