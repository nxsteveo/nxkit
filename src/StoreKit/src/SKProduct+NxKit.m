//
//  SKProduct+NxKit.m
//  NxKit
//
//  Created by Steve Wilford on 16/02/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import "SKProduct+NxKit.h"

@interface SKProduct ()
@property (nonatomic, strong) NSNumberFormatter *nx_priceFormatter;
@end

@implementation SKProduct (NxKit)

- (NSString *)nx_localizedPrice
{
    if (!self.nx_priceFormatter)
    {
        self.nx_priceFormatter = NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [self.nx_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [self.nx_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    }
    
    [self.nx_priceFormatter setLocale:self.priceLocale];
    return [self.nx_priceFormatter stringFromNumber:self.price];
}

@end
