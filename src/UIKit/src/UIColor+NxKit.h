//
//  UIColor+NxKit.h
//  NxKit
//
//  Created by Steve Wilford on 07/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (NxKit)

/**
 *  Creates and returns a color object using the specified opacity and RGB component values.
 
 *  Values above 255 are interpreted as 255.
 *
 *  @param red   The red component of the color object, specified as a value from 0 to 255.
 *  @param green The green component of the color object, specified as a value from 0 to 255.
 *  @param blue  The blue component of the color object, specified as a value from 0 to 255.
 *  @param alpha The opacity value of the color object, specified as a value from 0.0 to 1.0.
 *
 *  @return The color object. The color information represented by this object is in the device RGB colorspace.
 */
+ (UIColor *)nx_colorWithRed:(NSUInteger)red
                       green:(NSUInteger)green
                        blue:(NSUInteger)blue
                       alpha:(CGFloat)alpha;

/**
 *  Creates and returns a color object using the specified opacity and RGB component values.
 
 *  Values above 255 are interpreted as 255.
 *
 *  @param rgbHexString The red/green/blue components of the color object. This follows similar rules as CSS; it may be prefixed with #. It must comprise 6 characters. Examples: #FF0000 would be 100% red, #00FF00 would be 100% green etc.
 *  @param alpha        The opacity value of the color object, specified as a value from 0.0 to 1.0.
 *
 *  @return The color object. The color information represented by this object is in the device RGB colorspace.
 */
+ (UIColor *)nx_colorWithRGBHexString:(NSString *)rgbHexString alpha:(CGFloat)alpha;

@end
