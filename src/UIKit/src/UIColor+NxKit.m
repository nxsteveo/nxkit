//
//  UIColor+NxKit.m
//  NxKit
//
//  Created by Steve Wilford on 07/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import "UIColor+NxKit.h"

@implementation UIColor (NxKit)

+ (UIColor *)nx_colorWithRed:(NSUInteger)red
                       green:(NSUInteger)green
                        blue:(NSUInteger)blue
                       alpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:red / 255.0f
                           green:green / 255.0f
                            blue:blue / 255.0f
                           alpha:alpha];
}

+ (UIColor *)nx_colorWithRGBHexString:(NSString *)rgbHexString alpha:(CGFloat)alpha
{
    // Remove the leading # if present
    if ([rgbHexString hasPrefix:@"#"])
    {
        rgbHexString = [rgbHexString substringFromIndex:1];
    }
    
    if ([rgbHexString length] >= 6)
    {
        unsigned int colors = 0;
        
        // Scan for hex colors
        [[NSScanner scannerWithString:rgbHexString] scanHexInt:&colors];
        
        // Extract each color component
        unsigned short red = (colors & 0xFF0000) >> 16;
        unsigned short green = (colors & 0x00FF00) >> 8;
        unsigned short blue = (colors & 0x0000FF);
        
        return [UIColor nx_colorWithRed:red green:green blue:blue alpha:alpha];
    }
    else
    {
        return [UIColor blackColor];
    }
}

@end
