//
//  UIStoryboard+NxKit.h
//  NxKit
//
//  Created by Steve Wilford on 06/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (NxKit)

/**
 *  Instantiates and returns the initial view controller in the view controller graph in the storyboard with the specified name.
 *
 *  @param storyboardName The name of the storyboard resource file without the filename extension. This method raises an exception if this parameter is nil.
 *
 *  @return The initial view controller in the storyboard.
 */
+ (UIViewController *)nx_instantiateInitialViewControllerInStoryboardNamed:(NSString *)storyboardName;

/**
 *  Instantiates and returns the view controller with the specified identifier in the storyboard with the specified name.
 *
 *  @param viewControllerIdentifier An identifier string that uniquely identifies the view controller in the storyboard file.
 *  @param storyboardName           The name of the storyboard resource file without the filename extension. This method raises an exception if this parameter is nil.
 *
 *  @return The view controller corresponding to the specified identifier string or nil if no view controller is associated with the string.
 */
+ (UIViewController *)nx_instantiateViewControllerWithIdentifier:(NSString *)viewControllerIdentifier
                                               inStoryboardNamed:(NSString *)storyboardName;

/**
 *  Instantiates and returns the initial view controller in the view controller graph in the storyboard with the specified name.
 *
 *  @param storyboardName The name of the storyboard resource file without the filename extension. This method raises an exception if this parameter is nil.
 *  @param bundleOrNil    The bundle containing the storyboard file and its related resources. If you specify nil, this method looks in the main bundle of the current application.
 *
 *  @return The initial view controller in the storyboard.
 */
+ (UIViewController *)nx_instantiateInitialViewControllerInStoryboardNamed:(NSString *)storyboardName
                                                                  inBundle:(NSBundle *)bundleOrNil;

/**
 *  Instantiates and returns the view controller with the specified identifier in the storyboard with the specified name.
 *
 *  @param viewControllerIdentifier An identifier string that uniquely identifies the view controller in the storyboard file.
 *  @param storyboardName           The name of the storyboard resource file without the filename extension. This method raises an exception if this parameter is nil.
 *  @param bundleOrNil              The bundle containing the storyboard file and its related resources. If you specify nil, this method looks in the main bundle of the current application.
 *
 *  @return The view controller corresponding to the specified identifier string or nil if no view controller is associated with the string.
 */
+ (UIViewController *)nx_instantiateViewControllerWithIdentifier:(NSString *)viewControllerIdentifier
                                               inStoryboardNamed:(NSString *)storyboardName
                                                        inBundle:(NSBundle *)bundleOrNil;

@end
