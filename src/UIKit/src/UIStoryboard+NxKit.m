//
//  UIStoryboard+NxKit.m
//  NxKit
//
//  Created by Steve Wilford on 06/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import "UIStoryboard+NxKit.h"

@implementation UIStoryboard (NxKit)

+ (UIViewController *)nx_instantiateInitialViewControllerInStoryboardNamed:(NSString *)storyboardName
{
    return [self nx_instantiateInitialViewControllerInStoryboardNamed:storyboardName
                                                             inBundle:nil];
}

+ (UIViewController *)nx_instantiateInitialViewControllerInStoryboardNamed:(NSString *)storyboardName
                                                                  inBundle:(NSBundle *)bundleOrNil
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName
                                                         bundle:bundleOrNil];
    return [storyboard instantiateInitialViewController];
}

+ (UIViewController *)nx_instantiateViewControllerWithIdentifier:(NSString *)viewControllerIdentifier
                                               inStoryboardNamed:(NSString *)storyboardName
{
    return [self nx_instantiateViewControllerWithIdentifier:viewControllerIdentifier
                                          inStoryboardNamed:storyboardName
                                                   inBundle:nil];
}

+ (UIViewController *)nx_instantiateViewControllerWithIdentifier:(NSString *)viewControllerIdentifier
                                               inStoryboardNamed:(NSString *)storyboardName
                                                        inBundle:(NSBundle *)bundleOrNil
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName
                                                         bundle:bundleOrNil];
    return [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
}

@end
