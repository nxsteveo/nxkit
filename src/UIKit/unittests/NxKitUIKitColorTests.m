//
//  NxKitUIKitColorTests.m
//  NxKit
//
//  Created by Steve Wilford on 07/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UIColor+NxKit.h"

@interface NxKitUIKitColorTests : XCTestCase

@end

@implementation NxKitUIKitColorTests

- (void)testRGBColor
{
    NSUInteger inRed    = 255;
    NSUInteger inGreen  = 100;
    NSUInteger inBlue   = 50;
    CGFloat inAlpha     = 0.5f;
    
    UIColor *color = [UIColor nx_colorWithRed:inRed green:inGreen blue:inBlue alpha:inAlpha];
    
    if (color == nil)
    {
        XCTFail(@"Failed to create color");
    }
    else
    {
        BOOL pass = [self doesColor:color
                  matchColorWithRed:inRed
                              green:inGreen
                               blue:inBlue
                              alpha:inAlpha];
        
        if (pass == NO)
        {
            XCTFail(@"Failed to create or match hex color");
        }
    }
}

- (void)testHexColorStringWithoutPrefix
{
    NSString *hexString = @"FFFFFF";
    UIColor *color = [UIColor nx_colorWithRGBHexString:hexString alpha:1.0f];
    
    if (color == nil)
    {
        XCTFail(@"Failed to create color for string: %@", hexString);
    }
    else
    {
        [self doesColor:color
      matchColorWithRed:0xFF
                  green:0xFF
                   blue:0xFF
                  alpha:1.0f];
    }
}

- (void)testHexColorStringWithPrefix
{
    NSString *hexString = @"#FFFFFF";
    UIColor *color = [UIColor nx_colorWithRGBHexString:hexString alpha:1.0f];
    
    if (color == nil)
    {
        XCTFail(@"Failed to create color for string: %@", hexString);
    }
    else
    {
        [self doesColor:color
      matchColorWithRed:0xFF
                  green:0xFF
                   blue:0xFF
                  alpha:1.0f];
    }
}

- (void)testHexColorStringA
{
    NSString *hexString = @"#DD0000";
    UIColor *color = [UIColor nx_colorWithRGBHexString:hexString alpha:1.0f];
    
    if (color == nil)
    {
        XCTFail(@"Failed to create color for string: %@", hexString);
    }
    else
    {
        [self doesColor:color
      matchColorWithRed:0xDD
                  green:0x00
                   blue:0x00
                  alpha:1.0f];
    }
}

- (void)testHexColorStringB
{
    NSString *hexString = @"#00AA00";
    UIColor *color = [UIColor nx_colorWithRGBHexString:hexString alpha:1.0f];
    
    if (color == nil)
    {
        XCTFail(@"Failed to create color for string: %@", hexString);
    }
    else
    {
        [self doesColor:color
      matchColorWithRed:0x00
                  green:0xAA
                   blue:0x00
                  alpha:1.0f];
    }
}

- (void)testHexColorStringC
{
    NSString *hexString = @"#000077";
    UIColor *color = [UIColor nx_colorWithRGBHexString:hexString alpha:1.0f];
    
    if (color == nil)
    {
        XCTFail(@"Failed to create color for string: %@", hexString);
    }
    else
    {
        [self doesColor:color
      matchColorWithRed:0x00
                  green:0x00
                   blue:0x77
                  alpha:1.0f];
    }
}

- (void)testHexColorStringD
{
    NSString *hexString = @"#330033";
    UIColor *color = [UIColor nx_colorWithRGBHexString:hexString alpha:0.5f];
    
    if (color == nil)
    {
        XCTFail(@"Failed to create color for string: %@", hexString);
    }
    else
    {
        [self doesColor:color
      matchColorWithRed:0x33
                  green:0x00
                   blue:0x33
                  alpha:0.5f];
    }
}

- (BOOL)doesColor:(UIColor *)color
matchColorWithRed:(NSUInteger)red
            green:(NSUInteger)green
             blue:(NSUInteger)blue
            alpha:(CGFloat)alpha
{
    CGFloat outRed = 0.0f;
    CGFloat outGreen = 0.0f;
    CGFloat outBlue = 0.0f;
    CGFloat outAlpha = 0.0f;
    
    if ([color getRed:&outRed green:&outGreen blue:&outBlue alpha:&outAlpha])
    {
        // Confirm each color component matches
        BOOL redMatches = (NSUInteger)(outRed * 255) == red;
        BOOL greenMatches = (NSUInteger)(outGreen * 255) == green;
        BOOL blueMatches = (NSUInteger)(outBlue * 255) == blue;
        BOOL alphaMatches = outAlpha == alpha;
        
        XCTAssertTrue(redMatches, @"Red does not match");
        XCTAssertTrue(greenMatches, @"Green does not match");
        XCTAssertTrue(blueMatches, @"Blue does not match");
        XCTAssertTrue(alphaMatches, @"Alpha does not match");
        
        return (redMatches && greenMatches && blueMatches && alphaMatches);
    }
    else
    {
        return NO;
    }
}

@end
