//
//  NxKitUIKitStoryboardTests.m
//  NxKit
//
//  Created by Steve Wilford on 06/01/2014.
//  Copyright (c) 2014 Steve Wilford. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UIStoryboard+NxKit.h"

NSString * const NxStoryboardName = @"TestStoryboard";
NSString * const NxViewControllerIdentifier = @"DetailController";

@interface NxKitUIKitStoryboardTests : XCTestCase
@property (nonatomic, strong) NSBundle *bundle;
@end

@implementation NxKitUIKitStoryboardTests

- (void)setUp
{
    [super setUp];
    
    self.bundle = [NSBundle bundleForClass:[self class]];
}

- (void)testInitialViewControllerCreationFromSpecificBundle
{
    UIViewController *controller = [UIStoryboard nx_instantiateInitialViewControllerInStoryboardNamed:NxStoryboardName
                                                                                             inBundle:self.bundle];
    XCTAssertNotNil(controller, @"Failed to create controller");
    XCTAssertTrue([controller isKindOfClass:[UINavigationController class]], @"Failed to create correct type of controller");
}

- (void)testSpecificViewControllerCreationFromSpecificBundle
{
    UIViewController *controller = [UIStoryboard nx_instantiateViewControllerWithIdentifier:NxViewControllerIdentifier
                                                                          inStoryboardNamed:NxStoryboardName
                                                                                   inBundle:self.bundle];
    
    XCTAssertNotNil(controller, @"Failed to create controller");
    XCTAssertTrue([controller isMemberOfClass:[UIViewController class]], @"Failed to create correct type of controller");
}

@end
